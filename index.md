# Vincent Agnano


## About

I am 32 years old, from Montpellier (south of France), co-founder of
 [scopyleft](http://scopyleft.fr/en) and working there as web developer.


### Web development

I mostly do Python/Django, JavaScript and some frontend integration. I also
(rarely) write Ruby/Rails. I favour open-source and I am a best practices maniac.

French is my mother language. Though I speak English fluently, Italian average and
I used to speak Dutch too a while ago.

More here:

- Some code here: https://github.com/vinyll and https://github.com/scopyleft
- and a poor blog: http://vinyll.scopyleft.fr


### Contact

- email: vincent.agnano@scopyleft.fr
- phone: +33 (0) 4 84 250 412
- Twitter: https://twitter.com/vinyll

---

## Experience

### Open source projects

Author of
 [django-model-urls](https://github.com/vinyll/django-model-urls),
 [django-imagefit](https://github.com/vinyll/django-imagefit),
 [blaggr (NodeJS blog aggregator)](https://github.com/scopyleft/blaggr),
 [IsThatOnlyMe](https://github.com/scopyleft/isthatonlyme),
 [Worktimer (JavaScript)](https://github.com/vinyll/worktimer.titanium),
 [rails-mediabrowser](https://github.com/vinyll/rails-mediabrowser),
 [redmine-workload](https://github.com/MPPI-DPN/redmine-workload),
 [Web-5 website](https://github.com/scopyleft/web5),
 …

Developer on [Olympia](https://github.com/mozilla/olympia) (https://addons.mozilla.org).

Co-author of [Phase](https://github.com/Talengi/phase) (Django EDMS) and a few
other projects/libs/plugins.


### Closed source projects

[Roquefort-société](http://roquefort-societe.com/) (Django),
[Seaquarium](http://www.seaquarium.fr/) (Django),
[Loans for Banque Populaire](http://www.pretsetudiantsbanquepop.com/) (Django),
[Odysseum shopping center](http://www.centre-commercial-odysseum.com/) (symfony),
…


### Conferences

- Co-organizer and speaker at conferences such as
[web-5 2013](http://web-five.org) (Python and JavaScript, an efficient duo),
symfony Montpellier 2008 (Live code a full working app within one hour).


### Others

Proudly awarded of [many Mozilla badges](https://badges.mozilla.org/en-US/profiles/profile/vincent.agnano) :)

I also train teams for HTML5/CSS3/JS integration best practices and trained
in PHP, symfony, Flash/ActionScript, for universities, web training centers
and private companies (notice I dropped PHP, symphony and Flash).

---

## Timeline

- **2013 > today**: Co-founder and web dev at scopyleft _Montpellier, France_
- **2013 > 2014**: Web developer at Mozilla on [Mozilla addons](https://github.com/mozilla/olympia) _Remote_
- **2004 > 2012**: Self-employed web developer _Montpellier, France_
- **1999 > 2003**: Flight attendant for Skyjet and Qatar Airways _Belgium and Qatar_
- **1998 > 1999**: Private pilote license _Charleroi, Belgium_
- **1998**: Graduated from international school _SHAPE, Belgium_

---

## Activities

I practice martial arts (Kung-fu, Taekwondo), climbing, a little bit of music,
drawing, sculpting and all those things apart from code that make the every day life
even nicer.
